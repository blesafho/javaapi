FROM maven:3.3.9-jdk-8 as build

WORKDIR /app

COPY pom.xml .
COPY src src
RUN mvn clean install -DskipTests

RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)

FROM openjdk:8-jdk-alpine
#RUN apk add dumb-init
#RUN addgroup --system javauser && adduser -S -s /bin/false -G javauser javauser
WORKDIR /app
VOLUME /tmp
ARG TARGET=/app/target/
COPY --from=build ${TARGET}/lib /app/lib
COPY --from=build ${TARGET}/classes .

#RUN chown -R javauser:javauser /app
#USER javauser

EXPOSE 8080
#CMD "dumb-init" "java" "-cp" "/app:/app/lib/*" "com.dekses.jersey.docker.demo.Main"
ENTRYPOINT ["java","-cp","/app:/app/lib/*","com.dekses.jersey.docker.demo.Main"]


#-------------------------------------------------


#bitbucket
#FROM openjdk:8-jdk-alpine
#WORKDIR /app
#VOLUME /tmp
#COPY target/lib /app/lib
#COPY target/classes .

#EXPOSE 8080
#ENTRYPOINT ["java","-cp","/app:/app/lib/*","com.dekses.jersey.docker.demo.Main"]